package com.letstart.gitapifrag.adapter;


import android.widget.AbsListView;

public abstract class AbEndlessAdapter  implements AbsListView.OnScrollListener, ILoadAgain{

    private static final String TAG = "AbEndlessAdapter" ;
    int pagedNumberItems = 10;
    int currentPage = 0;
    int totNumItems = 0;
    private boolean inLoading = false;
    private int startingCallItem = 0;


    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }



    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount < totNumItems) {
            this.currentPage = this.startingCallItem;
            this.totNumItems = totalItemCount;
            if (totalItemCount == 0) {
                this.inLoading = true;
            }

        }

        if (inLoading && (totalItemCount > totNumItems)) {
            inLoading = false;
            totNumItems = totalItemCount;
            currentPage++;

        }

        int firsPlusPaged = firstVisibleItem+pagedNumberItems;
        int totalLessVisible = totalItemCount -visibleItemCount;

        if (!inLoading &&(totalLessVisible<= firsPlusPaged )){
            loadAgain(currentPage + 1, totalItemCount);
            inLoading = true;
        }


    }


}
