package com.letstart.gitapifrag.adapter;

public interface ILoadAgain {
    void loadAgain(int pageNeeded, int totalItemCount);
}
